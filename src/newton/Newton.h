/*
 * NewtonInterpolation.H
 *
 *  Created on: May 7, 2013
 *      Author: kloimuellner
 */

#ifndef NEWTONINTERPOLATION_H_
#define NEWTONINTERPOLATION_H_

#include <vector>

using namespace std;

class Newton {

private:
	/// given x-coordinates
	vector<double> x;
	/// given y-coordinates
	vector<double> y;
	/// calculated coefficients of Newton interpolation
	vector<double> c;

	/// result: interpolated function
	vector<double> *b;

	vector<double> horner(int, vector<double>);
	double doUntilConvergence(double value, vector<double> firstDerivative, vector<double> secondDerivative);
	double f(vector<double> func, double value);
	double round(double value, int places=0);

	bool debug;

	void printTerms(vector<double> terms) {
		int tSize = terms.size();

		cout << "[";
		for(int i = 0; i < tSize; i++) {
			cout << terms[i] << ", ";
		}
		cout << "]" << endl;
	};

public:
	Newton() : debug(false), b((vector<double> *) 0) {}

	Newton(bool _debug) : debug(_debug), b((vector<double> *) 0) {}

	void add(double x_i, double fx_i);
	double gamma(int i_0, int i_n);
	vector<double> getInterPolynom();
	vector<double> horner();
	vector<double> derivate(vector<double> terms);
	set<double> calculateExtremes();

	bool isDebug();
	void setDebug(bool _debug);
};

#endif /* NEWTONINTERPOLATION_H_ */
